<!DOCTYPE html>
<html>
  <head>
    <title>Rust iterators</title>
    <meta charset="utf-8">
    <link rel="icon" sizes="any" type="image/svg+xml" href="rust.svg">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Karla);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Karla'; }
      h1, h2, h3 {
        font-family: 'Karla';
        font-weight: normal;
      }
      .remark-code, .remark-inline-code {
        font-family: 'Ubuntu Mono';
        font-size: 24px;
      }
      code { background-color: #ddd; }
      .remark-slide-content {
        font-size: 24px;
        background-image: url("collabora.svg");
        background-position: 4% 96%;
        background-size: 10%;
      }
      #area {
        display: block;
        margin: auto;
        width: 100%;
        height: 400px;
      }
      #teaser {
        display: block;
        margin: auto;
        height: 200px;
      }
    </style>
  </head>
  <body>
    <textarea id="source">

class: center, middle

# Rust iterators

Antonio Caggiano

Collabora Ltd.

---

# It is a trait

```rust
pub trait Iterator {
    type Item;

    fn next(&mut self) -> Option<Self::Item>;

    /* ... */
}
```

`next()` Returns `Some(item)` until iteration is finished, when it returns `None`.

---

# How you get it

Common functions collections implement which return an iterator.

||||
|-|-|-|
|`iter()` |->| `for x in &v` |
|`iter_mut()` |->| `for x in &mut v` |
| `into_iter()` |->| `for x in v` |

---

# collect()

Iterators are lazy.

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().collect::<Vec<&i32>>(); // [&1, &2, &3, &4, &5]
```

---

# count()

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().count(); // 5
```

---

# last()

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().last(); // Some(&5)
```

---

# nth(n)

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().nth(2); // Some(&3)
```

---

# enumerate()

Creates an iterator which gives the current index as well as the value.

```rust
let v = vec![1, 2, 3, 4, 5];

for (i, v) in v.iter().enumerate() {
  println!("index {}: value {}", i, v);
}
```

---

# max() and min()

```rust
let v = vec![1, -2, 3, 4, -5];

v.iter().max(); // Some(&4)
v.iter().min(); // Some(&-5)
```

More:
- `max_by(compare)` and `min_by(compare)`
- `max_by_key(f)` and `min_by_key(f)`

---

# find(p)

Look for an element which satisfies a certain condition.

```rust
let v = vec![1, 2, 3, 4, 5];

v.iter().find(|&&x| x == 3); // Some(&3)
v.iter().find(|&&x| x == 0); // None
```

---

# position(p) and rposition(p)

Get the _position_ of an element which satisfies a certain condition.

```rust
let v = vec![1, 2, 3, 4, 5];

v.iter().position(|&x| x == 3); // Some(2)
v.iter().position(|&x| x == 0); // None
```

---

# sum()

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().sum::<i32>(); // 15
```

---

# product()

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().product::<i32>(); // 120
```

---

# reduce(f)

Reduce elements to a single one.

```rust
let v = vec![1, 2, 3, 4, 5];

v.into_iter().reduce(|a, b| a + b - 1); // Some(11)
```

---

# fold(base, f)

Like reduce but with a starting value

```rust
let v = vec![1, 2, 3, 4, 5];

v.into_iter().fold(2, |a, b| a + b); // 17
```

---

# map(f)

Returns one item for each element.

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().map(|&x| x + 1)
  .collect::<Vec<i32>>(); // [2, 3, 4, 5, 6]
```

---

# filter(p)

Iterates over those elements that satisfy a certain condition.

```rust
let v = vec![1, 2, 3, 4, 5];

v.iter().filter(|&x| x % 2 == 0)
  .collect::<Vec<&i32>>(); // [2, 4]
```

---

# filter_map(f)

Chains a filter and a map together.

The closure returns `None` to skip elements.

```rust
let v = vec![1, 2, 3, 4, 5];

v.iter()
    .filter_map(|&x| {
        if x % 2 == 0 {
            Some(x * x)
        } else {
            None
        }
    })
    .collect::<Vec<i32>>(); // [4, 16]
```

---

# all(f)

```rust
let v = vec![1, 2, 3, 4, 5];

v.iter().all(|&x| x < 6); // true
v.iter().all(|&x| x < 3); // false
```

---

# any(f)

```rust
let v = vec![1, 2, 3, 4, 5];

v.iter().any(|&x| x < 3); // true
v.iter().any(|&x| x < 0); // false
```

---

# rev()

If the iterator is a `DoubleEndedIterator`, it reverses it, starting from the end towards the beginning.

---

# cycle()

- Starts again from the beginning.
- Does not return `None` at the end, it returns the first element directly.

---

# cmp(other)

Compares lexicographically

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().cmp(v.iter()); // Ordering::Equal

let t = vec![2, 3, 4, 5, 6];
v.iter().cmp(t.iter()); // Ordering::Less
```

More:
- `eq`, `ne`, `lt`, `le`, `gt`, `ge`

---

# step_by(n)

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().step_by(2).collect(); // vec![1, 3, 5]
```

---

# partition(f)

```rust
let v = vec![1, 2, 3, 4, 5];

let (a, b): (Vec<&i32>, Vec<&i32>) =
    v.iter().partition(|&&x| x < 3);

// a -> [&1, &2]
// b -> [&3, &4, &5]
```

---

# chain(other)

Concatenates two iterators.

```rust
let a = vec![1, 2, 3];
let b = vec![4, 5, 6];

a.iter().chain(b.iter())
  .collect::<Vec<&i32>>(); // [&1, &2, &3, &4, &5, &6]
```

---

# peekable()

Look at the next element without advancing.

```rust
let v = vec![1, 2, 3, 4, 5];
let mut iter = v.iter().peekable();

iter.peek(); // Some(&&1)
iter.next(); // Some(&1)
```

---

# skip(n) and take(n)

- Skips or the first `n` elements.

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().skip(3).collect::<Vec<&i32>>(); // [&4, &5]
```

- Takes the first `n` elements.

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().take(2).collect::<Vec<&i32>>(); // [&1, &2]
```

More:
- `skip_while(p)` and `take_while(p)`

---

# scan(init_state, f)

Maintains a state while iterating.

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter().scan(1, |state, &x| {
        *state = *state + x;

        Some(-*state)
    })
    .collect::<Vec<i32>>(); // [-2, -4, -7, -11, -16]
```

---

# flatten()

- Iterator of iterators -> iterator.
- Iterator of something that can be iterated -> iterator.

```rust
let v = vec![vec![1, 2, 3], vec![4, 5]];
v.into_iter().flatten()
  .collect::<Vec<i32>>(); // [1, 2, 3, 4, 5];
```

More:

- `flat_map(f)`
  - The closure returns an iterator for each element.
  - Equivalent to `map(f).flatten()`.

---

# fuse()

- After an iterator returns `None`, calling `next()` may return `Some(T)` again.

- `fuse()` ensures it ends after the first `None`. In other words, future calls to `next()` return always `None`.

---

# inspect(f)

Does something with each element. Useful for debugging.

```rust
let v = vec![1, 2, 3, 4, 5];
v.iter()
    .inspect(|&x| print!("{}", x))
    .map(|&x| x + 1)
    .inspect(|&x| println!("-> {}", x))
    .collect::<Vec<i32>>(); // [2, 3, 4, 5, 6]
```

```sh
# stdout
1 -> 2
2 -> 3
3 -> 4
4 -> 5
5 -> 6
```

---

# zip(other)

Two iterators -> one iterator of pairs.

```rust
let a = vec![1, 2, 3];
let b = vec!["a", "b", "c"];

a.iter().zip(b.iter())
    .collect::<Vec<(&i32, &&str)>>();
    // [(1, "a"), (2, "b"), (3, "c")]
```

---

# unzip()

An iterator of pairs -> pair of containers.

```rust
let v = vec![(1, "a"), (2, "b"), (3, "c")];

let (a, b): (Vec<i32>, Vec<&str>) =
    v.into_iter().unzip();
// a: [1, 2, 3]
// b: ["a", "b", "c"]
```

---

# copied() (or cloned())

- Copies (or clones) all the elements.
- Element must implement `Copy` (or `Clone`) trait.

```rust
// copied() equivalent
v.iter().map(|&x| x).collect();

// cloned() equivalent
v.iter().map(|x| x.clone()).collect();
```

---

# References

- https://doc.rust-lang.org/book/ch13-02-iterators.html
- https://doc.rust-lang.org/std/iter/trait.Iterator.html

---

class: center, middle

# Thanks!

    </textarea>
    <script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
    <script>var showcase = remark.create();</script>
  </body>
</html>
